<?php

$searchRoot = '/home/antimony/Desktop/aleksandr-strelkov/test_search';
$searchName = 'test.txt';
$searchResult = [];

function searchFile (string $searchRoot, string $searchName, array &$searchResult)
{
    $elements = array_diff(scandir($searchRoot), array('..', '.'));
//    print_r($elements);
    foreach ($elements as &$element) {
        if (is_dir($searchRoot.'/'.$element)) {
            searchFile ($searchRoot.'/'.$element, $searchName, $searchResult);
        } elseif ($element == $searchName) {
            $searchResult [] = $searchRoot.'/'.$element;
        }
    }
    unset($element);
}

searchFile($searchRoot, $searchName, $searchResult);

if (array_filter($searchResult, "filesize")) {
    print_r(array_filter($searchResult, "filesize"));
} else {
    echo 'Поиск не дал результатов'.PHP_EOL;
}